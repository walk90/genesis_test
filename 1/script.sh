#!/bin/bash
[[ "$#" != "1" ]] && echo "Use only terraform or ansible arguments." && exit 1

AAKI=""							# AWS_ACCESS_KEY_ID
ASAK=""							# AWS_SECRET_ACCESS_KEY
tag1="terraform:kosenko"
tag2="aws-cli:ansible"

if [[ "$1" == "terraform" ]]; then
		             docker images --format '{{.Repository}}:{{.Tag}}' | grep ${tag1} || docker build -t ${tag1} -f Dockerfile_terraform .
			     docker run -it -v "$PWD"/terraform:/terraform \
				            -e TERI='terraform init' \
					    ${tag1} tinit
			     docker run -it -v "$PWD"/terraform:/terraform \
				            -e AWS_ACCESS_KEY_ID="${AAKI}" \
				            -e AWS_SECRET_ACCESS_KEY="${ASAK}" \
					    -e TERP='terraform plan' \
					    ${tag1} tplan
			     docker run -it -v "$PWD"/terraform:/terraform \
				     	    -e AWS_ACCESS_KEY_ID="${AAKI}" \
					    -e AWS_SECRET_ACCESS_KEY="${ASAK}" \
					    -e TERA='terraform apply' \
					    ${tag1} tapply		     
elif [[ "$1" == "ansible" ]]; then
                             docker images --format '{{.Repository}}:{{.Tag}}'| grep ${tag2} || docker build -t ${tag2} -f Dockerfile_aws-ansible .
			     IP=$(docker run -it -v $PWD/ansible:/ansible \
									-v $PWD/ssh/:/root/.ssh \
									-e AWS_ACCESS_KEY_ID="${AAKI}" \
									-e AWS_SECRET_ACCESS_KEY="${ASAK}" \
									-e AWS_DEFAULT_REGION='eu-central-1' \
									${tag2} "aws ec2 describe-instances --query 'Reservations[*].Instances[*].PublicIpAddress' --output=text | sed 2d")
			     sed "s/^ec1.*/ec1 ansible_host=$IP/" -i ansible/hosts
			     SSHKEY="/root/.ssh/id_rsa"                               	        			# Path to private ssh-key.
			     USER="ubuntu"			     	               					# Ansible user.
			     docker run -it -v $PWD/ansible:/ansible \
									-v $PWD/ssh/:/root/.ssh \
									-e ANSIBLE_HOST_KEY_CHECKING='False' \
									-e ANSIBLE_PRIVATE_KEY_FILE="${SSHKEY}" \
									-e ANSIBLE_REMOTE_USER="${USER}" \
									${tag2} 'ansible-playbook -i hosts playbook.yml'
else
echo "Use only terraform or ansible arguments."
fi
