variable region {
  default = "eu-central-1"
}

variable ami {
  default = "ami-0d359437d1756caa8"
}

variable instance_type {
  default = "t2.micro"
}

variable public_key {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDoYU4Fu4Vn8ZAxWsvsjcidl93D96UD4VyySi515TSjVtb5r0Ih39tCdtOg0OYTJm4X8g45088lPF6NabArs0JhrU5DfDjS0kpIvGoC6CSTWp53lGtY8xJw6ULQ1zZRqyPHijDRfJGC+lLaN6XMfAMGhv0IqMjPySSdi3Ooi5chAGRGK1p6nsyUz1c0CzO4ORHYFr+7ORXrHycpi2mMUiczNbTCholDDJJdsYK4Xi8UUF/pM3RF2NCOCLoghzsCZXmDEB2TUB6uL8k9FO6i/tEHTtfUbdEDEsKWXU8EUCcTXo/JWycn/oMII/2drs56ziBTmAnGpY1E+fQr+2LeZ162jQpkd3IC6b1xO9q2QQqnKqSTBCl3pwdRKfckavkzjvNYmA8ink0V3cYzL0f+Z/3R/64lyQUVEnImopjNrLagWipSHDtmKA5dJIIcde44oVx+tEAzDoGynFV4TBsFWUUKt5fx4ZGOej46J8iUl9fZz+zaoAmf5aGk25TYJO13XbE= walk90@walk90-HP-EliteBook-840-G3"
}
