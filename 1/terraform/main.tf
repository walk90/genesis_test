provider "aws" {
        region = "${var.region}"
}

resource "aws_instance" "ec2_instance" {
   ami = "${var.ami}"
   instance_type = "${var.instance_type}"
   vpc_security_group_ids = [aws_security_group.ec2_instance.id]
   key_name = "terraform_ec2_key"
}

resource "aws_security_group" "ec2_instance" {
  name        = "TEST"
  description = "Security group for Webserver"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_key_pair" "terraform_ec2_key" {
  key_name = "terraform_ec2_key"
  public_key = "${var.public_key}"
}

