Test task for DevOps Engineer
In two parts:
1. Create a bash/python scenario, which dependently on taken argument starting two different custom
Docker images in interactive mode (Use your preferred CLI interpreter).
• Ansible container must include:
o Latest stable versions of Ansible and AWS CLI.
o Your most widely used CLI tools.
• Automatically assign environment values:
o A couple of typical Ansible related variables.
o One Ansible quick run alias/function.
o AWS CLI authentication keys.
• Pass as volumes:
o Directory with any simple Ansible playbook, role, and inventory set.
o Directory with SSH key for Ansible remote access.
• Terraform container must include:
o Latest stable version of Terraform, compiled from source.
o Your most widely used CLI tools
• Define environment values for:
o Terraform AWS authentication data.
o Quick aliases for "terraform init", "terraform plan" and "terraform apply"
• Pass as volume:
o Directory with terraform resources with minimal and simplest AWS configuration.
2. Create a docker-compose file, which runs NginX+PHP-FPM+(Redis or Memcached) as separate services
in a common network.
• Only Nginx must be available outside.
• phpinfo() as index page on default virtual host.
