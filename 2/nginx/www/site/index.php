<?php
$mem = new Memcached();
$mem->addServer("memcached", 11211);
$result = $mem->get("key_name");
if ($result) {
    echo $result;
} else {
    echo "No key found. Adding key to cache.";
    $mem->set("key_name", "Key_name's value from memcached!") or die ("Couldn't save anything to memcached...");
}
?>
<?php
phpinfo();
